import { useState, useEffect, useContext } from 'react';

import UserContext from '../UserContext';

import { Navigate, useNavigate } from 'react-router-dom';

import { Form, Button } from 'react-bootstrap';

import Swal from 'sweetalert2';

export default function Register() {

    // Allows us to use the UserContext object and its properties to be used for user validation. 
    const { user } = useContext(UserContext);

    const navigate = useNavigate();

    // State hooks to store the values of the input fields
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    // State to determine whether submit button will be enabled or not
    const [isActive, setIsActive] = useState(false);
    const [itExists, setItExists] = useState(true);


    // Function to simulate user registration
    function registerUser(e) {
        // Prevents page from reloading
        e.preventDefault();

        if(!itExists){
            fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    firstName: firstName,
                    lastName: lastName,
                    mobileNo: mobileNo,
                    email: email,
                    password: password1
                })
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);

                Swal.fire({
                  title: "Registration successful",
                  icon: "success",
                  text: "Welcome to Zuitt!"
                });
                setFirstName('');
                setLastName('');
                setMobileNo('');
                setEmail('');
                setPassword1('');
                setPassword2('');
                navigate("/login");
            })
        } else {
            Swal.fire({
                title: "Duplicate email found",
                icon: "error",
                text: "Please provide a different email."
            });
            setEmail('');
        }
    }

    useEffect(() => {

        fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
            method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    email: email,
                })
        })
        .then(res => res.json())
        .then(data => {
            if(data == true){
                setItExists(true);
            } else {
                setItExists(false);
            }
        })

        // Validation to enable submit button when all fields are populated and both passwords match
        if((email !== '' && password1 !== '' && password2 !== '' && firstName !== '' && lastName !=='' && mobileNo !=='') && (password1 === password2) && mobileNo.length === 11){
            setIsActive(true);
        } else {
            setIsActive(false);
        }
    // Dependencies
    // No dependencies - effect function will run every time component renders
    // With dependency (empty array) - effect function will only run (one time) when the component renders
    // With dependencies - effect function will run anytime one of the values in the array of dependencies changes
    }, [email, password1, password2, firstName, lastName, mobileNo]);

    return (
        (user.id !== null) ?
        <Navigate to="/courses"/>
        :
        <Form onSubmit={(e) => registerUser(e)}>

            <Form.Group controlId="firstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control 
                    type="text" 
                    placeholder="First Name" 
                    value={firstName}
                    onChange={e => setFirstName(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="lastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control 
                    type="text" 
                    placeholder="Last Name" 
                    value={lastName}
                    onChange={e => setLastName(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email"
                    value={email}
                    onChange={e => setEmail(e.target.value)} 
                    required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="mobileNo">
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control 
                    type="text" 
                    placeholder="Mobile Number" 
                    value={mobileNo}
                    onChange={e => setMobileNo(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password" 
                    value={password1}
                    onChange={e => setPassword1(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Verify Password" 
                    value={password2}
                    onChange={e => setPassword2(e.target.value)}
                    required
                />
            </Form.Group>
            {/*conditionally render submit button based on "isActive" state*/}
            { isActive ?
            <Button variant="primary" type="submit" id="submitBtn">
                Submit
            </Button>
            :
            <Button variant="danger" type="submit" id="submitBtn">
                Submit
            </Button>
            }
            
        </Form>
    )
}